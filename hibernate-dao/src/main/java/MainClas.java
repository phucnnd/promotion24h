
import java.util.LinkedList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.promotion24h.dao.entity.Function;
import org.promotion24h.dao.entity.Role;

public class MainClas {
	public static void main(String[] args) {
		Session session = null;

		try {
			// This step will read hibernate.cfg.xml and prepare hibernate for
			// use
			SessionFactory sessionFactory = new AnnotationConfiguration().configure().buildSessionFactory();
			session = sessionFactory.openSession();
			Transaction trans = session.getTransaction();
			// Create new instance of Contact and set values in it by reading
			// them from form object
			/*trans.begin();
			System.out.println("Begining...");
			
			Role role = (Role) session.get(Role.class, 1);
			role.setFunctions(null);
 
			session.save(role);
			
			trans.commit();*/
			System.out.println("Done");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			// Actual contact insertion will happen at this step
			session.flush();
			session.close();

		}

	}
}
