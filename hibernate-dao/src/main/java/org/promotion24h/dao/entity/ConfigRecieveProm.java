package org.promotion24h.dao.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "CONFIG_RECEIVE_PROMOTION")
public class ConfigRecieveProm implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id; 
	
	@Column(name = "FROM_TIME")
	private Timestamp from;
	
	@Column(name = "TO_TIME")
	private Timestamp to; 
	
	@Column(name = "RECEIVE_BY")
	private String receive_by;
	
	@ManyToMany
	@JoinTable(name = "RECEIVE_PROM_OF_SHOP_TYPES", 
			joinColumns = {@JoinColumn(name = "CONFIG_RECEIVE_PROM_ID")},
			inverseJoinColumns = {@JoinColumn(name = "SHOP_ID")})
	private List<ShopType> shop_types;
	
	@Column(name = "WARD")
	private String ward;
	
	@Column(name = "DISTRICT")
	private String district;
	
	@Column(name = "CITY")
	private String city;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Timestamp getFrom() {
		return from;
	}

	public void setFrom(Timestamp from) {
		this.from = from;
	}

	public Timestamp getTo() {
		return to;
	}

	public void setTo(Timestamp to) {
		this.to = to;
	}

	public String getReceive_by() {
		return receive_by;
	}

	public void setReceive_by(String receive_by) {
		this.receive_by = receive_by;
	}

	public List<ShopType> getShop_types() {
		return shop_types;
	}

	public void setShop_types(List<ShopType> shop_types) {
		this.shop_types = shop_types;
	}

	public String getWard() {
		return ward;
	}

	public void setWard(String ward) {
		this.ward = ward;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
}
