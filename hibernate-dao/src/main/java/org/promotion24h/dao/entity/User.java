package org.promotion24h.dao.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@Table(name = "USERS")
public class User implements Serializable{
	@Id
	@Column(name = "USER_ID")
	private int id;
	
	@Column(name = "PASSWORD")
	private String pass;
	
	@Column(name = "STATUS")
	private String status;
	
	@OneToOne(optional = true, orphanRemoval=true)
	@Cascade({CascadeType.SAVE_UPDATE})
	private UserProfile profile;
	
	@ManyToOne(optional = false)
	@Cascade({CascadeType.SAVE_UPDATE})
	@JoinColumn(name = "ROLE_ID")
	private Role role;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "USER_FAVORITE_SHOPS", 
		joinColumns = {@JoinColumn(name = "USER_ID")},
		inverseJoinColumns = {@JoinColumn(name = "SHOP_ID")})
	private List<Shop> favoriteShops;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public void setProfile(UserProfile profile) {
		this.profile = profile;
	}
	public UserProfile getProfile() {
		return profile;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	public List<Shop> getFavoriteShops() {
		return favoriteShops;
	}
	public void setFavoriteShops(List<Shop> favoriteShops) {
		this.favoriteShops = favoriteShops;
	}
}
