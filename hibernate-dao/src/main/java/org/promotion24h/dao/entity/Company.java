package org.promotion24h.dao.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;


@Entity
@Table(name = "COMPANY")
public class Company implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@OneToOne(fetch=FetchType.EAGER, orphanRemoval=true)
	@Cascade({CascadeType.SAVE_UPDATE})
	@JoinColumn(name = "ADDRESS_ID")
	private Address address;
	
	@Column(name = "TAX_NUMBER")
	private String taxNumber;
	
	@Column(name = "COMPANY_NAME")
	private String name;
	
	@Column(name = "CONTRACT_NUMBER")
	private String contractNumber;
	
	@Column(name = "CONTRACT_DATE")
	private Date contractDate;
	
	@OneToOne
	@JoinColumn(name = "OWNER_BY")
	private User ownerBy;
		
	@OneToMany(mappedBy = "company", orphanRemoval = true)
	@Cascade({CascadeType.SAVE_UPDATE, CascadeType.DELETE})
	private List<Shop> shops;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getTaxNumber() {
		return taxNumber;
	}

	public void setTaxNumber(String taxNumber) {
		this.taxNumber = taxNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public Date getContractDate() {
		return contractDate;
	}

	public void setContractDate(Date contractDate) {
		this.contractDate = contractDate;
	}

	public User getOwnerBy() {
		return ownerBy;
	}

	public void setOwnerBy(User ownerBy) {
		this.ownerBy = ownerBy;
	}

	public List getShops() {
		return shops;
	}

	public void setShops(List shops) {
		this.shops = shops;
	}
	
	
}
