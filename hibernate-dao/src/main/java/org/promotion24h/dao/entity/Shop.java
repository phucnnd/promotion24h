package org.promotion24h.dao.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@Table(name="SHOP")
public class Shop implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "SHOP_NAME")
	private String name;
	
	@Column(name = "lon", precision=8)
	private double lon;
	
	@Column(name = "lat", precision=8)
	private double lat;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "SHOP_TYPE")
	private ShopType type;
	
	@ManyToOne
	@JoinColumn(name = "COMPANY_ID")
	private Company company;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "SHOP_PROMOTION", 
			joinColumns = {@JoinColumn(name = "SHOP_ID")},
			inverseJoinColumns = {@JoinColumn(name = "PROM_ID")})
	@Cascade({CascadeType.SAVE_UPDATE})
	private List<Promotion> promotions;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public ShopType getType() {
		return type;
	}

	public void setType(ShopType type) {
		this.type = type;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public List<Promotion> getPromotions() {
		return promotions;
	}

	public void setPromotions(List<Promotion> promotions) {
		this.promotions = promotions;
	}
	
	
}
